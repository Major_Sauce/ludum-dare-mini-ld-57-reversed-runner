package Bars;

import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Graphics.Tile;

public class Bar {

	private int currentValue;
	private int lastValue;
	private int minValue;
	private int maxValue;
	private BarType type;
	private Tile tile;

	public Bar(BarType type, int minValue, int maxValue, int currentValue) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.currentValue = currentValue;
		this.type = type;
		tile = buildBar();
	}

	public void update() {
		if (currentValue != lastValue) {
			lastValue = currentValue;
			int[] pixels = tile.getPixels();
			int currentBarHeight = (int)((double)(type.getHeight() - type.getBorderHeight() * 2) / (int)(Math.abs(maxValue - minValue)) * currentValue);
			for(int x = type.getBorderWidth(); x < type.getWidth() - type.getBorderWidth(); x++){
				for(int y = type.getBorderHeight(); y <= type.getHeight() - type.getBorderHeight(); y++){
					if(y < type.getHeight() - type.getBorderHeight() - currentBarHeight){
						pixels[x + y * type.getWidth()] = 0;
					} else {
						pixels[x + y * type.getWidth()] = type.getBarColor();
					}
				}
			}
		}
	}

	private Tile buildBar() {
		int width = type.getWidth();
		int height = type.getHeight();
		int[] pixels = new int[width * height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (x < type.getBorderWidth() || y < type.getBorderHeight() || x >= width - type.getBorderWidth() || y > height - type.getBorderHeight()) {
					pixels[x + y * width] = type.getBorderColor();
				}
			}
		}
		return new Tile(pixels, width, height);
	}

	public void renderBar(Screen screen, int x, int y) {
		screen.renderTile(tile, x, y);
	}

	// Getters
	public int getMinValue() {
		return minValue;
	}

	public void setValue(int value) {
		currentValue = value;
	}

	public int getMaxValue() {
		return maxValue;
	}

}
package Bars;

public enum BarType {
	
	HealthBar(8, 26, 3, 3, 16711680, 14329120);
	
	private int width;
	private int height;
	private int borderWidth;
	private int borderHeight;
	private int barColor;
	private int borderColor;

	private BarType(int width, int height, int borderWidth, int borderHeight, int barColor, int borderColor){
		this.width = width;
		this.height = height;
		this.barColor = barColor;
		this.borderColor = borderColor;
		this.borderWidth = borderWidth;
		this.borderHeight = borderHeight;
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}
	
	public int getBarColor(){
		return barColor;
	}
	
	public int getBorderColor(){
		return borderColor;
	}
	
	public int getBorderWidth(){
		return borderWidth;
	}
	
	public int getBorderHeight(){
		return borderHeight;
	}
}
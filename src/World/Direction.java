package World;

public enum Direction {

	NONE(0, 0), NORTH(0, -1), EAST(1, 0), SOUTH(0, 1), WEST(-1, 0);
	
	private int xMod;
	private int yMod;
	
	private Direction(int xMod, int yMod){
		this.xMod = xMod;
		this.yMod = yMod;
	}
	
	public int getXMod(){
		return xMod;
	}
	
	public int getYMod(){
		return yMod;
	}
	
	public static Direction toDirection(double x, double y){
		if(x == 0 && y== 0){
			return Direction.NONE;
		}
		if(Math.abs(x) > Math.abs(y)){
			if(x < 0){
				return Direction.WEST;
			} else {
				return Direction.EAST;
			}
		} else {
			if(y < 0){
				return Direction.NORTH;
			} else {
				return Direction.SOUTH;
			}
		}
//		for(Direction dir : values()){
//			if(dir.getXMod() == xMod || dir.getYMod() == yMod){
//				return dir;
//			}
//		}
	}
	
}

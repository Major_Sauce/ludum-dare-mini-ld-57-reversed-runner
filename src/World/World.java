package World;

import Unit.Unit;
import Unit.UnitType;
import Weapon.Weapon;
import de.Major_Sauce.Game.ScreenManager.Screen;

public class World {

	private int width;
	private int height;

	private Terrain[] terrain;
	private Unit[] units;
	private Weapon[] weapons;
	
	public World(int width, int height) {
		this.width = width;
		this.height = height;
		this.terrain = new Terrain[width * height];
		this.units = new Unit[width * height];
		this.weapons = new Weapon[width * height];

		// Setting terrain
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (x == 0 || y == 0 || x == width - 1 || y == height - 1) {
					if (x == 0 || x == width - 1) {
						terrain[x + y * width] = new Terrain(
								TerrainType.STONE_WALL_SIDES, x, y);
					} else if (y == height - 1) {
						terrain[x + y * width] = new Terrain(
								TerrainType.STONE_WALL_BOTTOM, x, y);
					} else {
						if (Math.random() > 0.5) {
							terrain[x + y * width] = new Terrain(
									TerrainType.MOSSY_STONE_WALL_TORCH, x, y);
						} else {
							terrain[x + y * width] = new Terrain(
									TerrainType.MOSSY_STONE_WALL, x, y);
						}
					}
				} else {
					double random = Math.random();
					if (random <= 0.1) {
						terrain[x + y * width] = new Terrain(
								TerrainType.STONE_WALL_BOTTOM, x, y);
					} else {
						terrain[x + y * width] = new Terrain(
								TerrainType.GROUND, x, y);
					}
				}
			}
		}
	}

	public void update(int timeSinceLastUpdate) {
		for (int i = 0; i < units.length; i++) {
			Unit unit = units[i];
			if (unit != null) {
				unit.tick(timeSinceLastUpdate);
				;
			}
		}
		for (Weapon weapon : weapons) {
			if (weapon != null) {
				weapon.tick(timeSinceLastUpdate);
			}
		}
		// p.tick(timeSinceLastUpdate);
	}

	public void render(Screen screen) {
		for (int i = 0; i < terrain.length; i++) {
			terrain[i].render(screen);
		}
		for (int i = 0; i < units.length; i++) {
			Unit unit = units[i];
			if (unit != null) {
				unit.render(screen);
			}
		}
		
		// p.render(screen);
		for (Weapon weapon : weapons) {
			if (weapon != null) {
				weapon.render(screen);
			}
		}
	}

	public void addUnit(Unit unit) {
		units[unit.getUnitId()] = unit;
	}

	public void removeUnit(Unit unit) {
		units[unit.getUnitId()] = null;
	}

	public void addWeapon(Weapon weapon) {
		weapons[weapon.getWeaponId()] = weapon;
	}

	public void removeWeapon(Weapon weapon) {
		weapons[weapon.getWeaponId()] = null;
	}

	// Getters and setters

	public Terrain getTerrainAt(int x, int y) {
		if (x < 0 || y < 0 || x >= width || y >= height) {
			return null;
		}
		return terrain[x + y * width];
	}

	public Unit[] getUnits() {
		return units;
	}

	public Unit getUnitAt(Location loc) {
		for (Unit unit : units) {
			if (unit != null) {
				Location location = unit.getLocation();
				if (location.getXAsInt() == loc.getXAsInt() && location.getYAsInt() == loc.getYAsInt()) {
					return unit;
				}
			}
		}
		return null;
	}

	public int getFreeUnitId() {
		for (int i = 0; i < units.length; i++) {
			if (units[i] == null) {
				return i;
			}
		}
		return 0;
	}

	public int getFreeWeaponId() {
		for (int i = 0; i < weapons.length; i++) {
			if (weapons[i] == null) {
				return i;
			}
		}
		return 0;
	}
	
	public void spawnEnemys(int count) {
		for (int i = 0; i < count; i++) {
			int x = 0;
			int y = 0;
			while (x == 0 || x == width - 1) {
				x = (int) (Math.random() * width);
			}
			while (y == 0 || y == height - 1) {
				y = (int) (Math.random() * height);
			}
			if(getFreeUnitId() >= units.length - 1){
				return;
			}
			addUnit(new Unit(UnitType.Barbarian, new Location(x, y)));
			System.out.println("Spawned enemy");
		}
	}

}

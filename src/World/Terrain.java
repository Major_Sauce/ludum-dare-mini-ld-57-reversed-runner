package World;

import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Units.Point;

public class Terrain {

	private Location location;
	private TerrainType type;
	
	public Terrain(TerrainType type, int x, int y){
		this.type = type;
		this.location = new Location(x, y);
	}

	public void tick(){
		
	}
	
	public void render(Screen screen){	
		screen.renderTile(type.getTexture(), location.toPixelPosX(), location.toPixelPosY());
	}
	
	//getter and setter
	public TerrainType getType(){
		return type;
	}
	
	public Point getLocation(){
		return location;
	}
	
}

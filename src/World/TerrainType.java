package World;

import Main.Main;
import Main.SystemData;
import de.Major_Sauce.Graphics.SpriteSheet;
import de.Major_Sauce.Graphics.Tile;

public enum TerrainType {

	STONE_WALL(0, true), STONE_WALL_SIDES(1, true), STONE_WALL_TORCH(2, true), MOSSY_STONE_WALL(3, true), MOSSY_STONE_WALL_TORCH(4, true), STONE_WALL_BOTTOM(5, true), GROUND(10, false);
	
	private Tile texture;
	private boolean solid;
	
	private TerrainType(int tileIndex, boolean solid){
		loadSpriteSheet();
		this.texture = getTexture(tileIndex);
		this.solid = solid;
	}

	public Tile getTexture(){
		return texture;
	}
	
	public boolean isSolid(){
		return solid;
	}
	
	//Static
	private static SpriteSheet terrainSheet;
	
	private static void loadSpriteSheet(){
		if(terrainSheet == null){
			terrainSheet = Main.spriteSheetManager.loadSpriteSheet("/textures/terrain.png", SystemData.tileWidth, SystemData.tileHeight);
			System.out.println("Terraint sheet loaded");
		}
	}
	
	public Tile getTexture(int index){
		return terrainSheet.getTile(index, SystemData.scale);
	}
	
}

package World;

import Main.SystemData;
import de.Major_Sauce.Units.Point;

public class Location extends Point {

	public Location(){
		setX(0);
		setY(0);
	}
	
	public Location(double x, double y) {
		setX(x);
		setY(y);
	}
	
	public int toPixelPosX(){
		return (int)(getX() * SystemData.tileWidth * SystemData.scale);
	}
	
	public int toPixelPosY(){
		return (int)(getY() * SystemData.tileWidth * SystemData.scale);	
	}
	
}

package Unit;

public abstract class UnitTypeLoader extends UnitAnimationContainer {

	@Override
	public abstract void loadAnimations();

}

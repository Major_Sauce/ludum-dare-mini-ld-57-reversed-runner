package Unit;

import de.Major_Sauce.Graphics.SpriteSheet;
import Animation.Animation;
import Main.Main;

public class BarbarianTypeLoader extends UnitTypeLoader {

	@Override
	public void loadAnimations() {
		final SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/units.png", 64, 64);
		

		idleDownAnimation = new Animation() {
			@Override
			public void loadAnimation() {
				setTicksPerUpdate(1);
				addTile(sheet.getTile(1));
			}
		};
		
		idleLeftAnimation = new Animation() {
			@Override
			public void loadAnimation() {
				setTicksPerUpdate(1);
				addTile(sheet.getTile(4));
			}
		};
		
		idleUpAnimation = new Animation() {
			@Override
			public void loadAnimation() {
				setTicksPerUpdate(1);
				addTile(sheet.getTile(10));
			}
		};
		
		idleRightAnimation = new Animation() {
			@Override
			public void loadAnimation() {
				setTicksPerUpdate(1);
				addTile(sheet.getTile(7));
			}
		};
		
		walkDownAnimation = new Animation() {
			@Override
			public void loadAnimation() {
				setTicksPerUpdate(17);
				addTile(sheet.getTile(0));
				addTile(sheet.getTile(1));
				addTile(sheet.getTile(2));
			}
		};

		walkLeftAnimation = new Animation() {
			@Override
			public void loadAnimation() {
				setTicksPerUpdate(17);
				addTile(sheet.getTile(3));
				addTile(sheet.getTile(4));
				addTile(sheet.getTile(5));	
			}
		};

		walkRightAnimation = new Animation() {
			@Override
			public void loadAnimation() {
				setTicksPerUpdate(17);
				addTile(sheet.getTile(6));
				addTile(sheet.getTile(7));
				addTile(sheet.getTile(8));		
			}
		};
		
		walkUpAnimation = new Animation() {
			@Override
			public void loadAnimation() {
				setTicksPerUpdate(17);
				addTile(sheet.getTile(9));
				addTile(sheet.getTile(10));
				addTile(sheet.getTile(11));
			}
		};
		
	}

}

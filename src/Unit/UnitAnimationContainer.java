package Unit;

import Animation.Animation;
import Animation.AnimationContainer;

public abstract class UnitAnimationContainer extends AnimationContainer {

	public Animation idleLeftAnimation;
	public Animation idleRightAnimation;
	public Animation idleUpAnimation;
	public Animation idleDownAnimation;
	
	public Animation walkLeftAnimation;
	public Animation walkRightAnimation;
	public Animation walkUpAnimation;
	public Animation walkDownAnimation;
	
	public Animation attackAnimation;
	public Animation defendAnimation;
	public Animation dieAnimation;
	
	@Override
	public abstract void loadAnimations();
	
}

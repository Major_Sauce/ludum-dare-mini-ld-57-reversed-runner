package Unit;

import Player.PlayerTypeLoader;

public enum UnitType {

	PLAYER(100, new PlayerTypeLoader()), Barbarian(100, new BarbarianTypeLoader());

	private int maxHealth;
	public UnitAnimationContainer animContainer;
	
	private UnitType(int maxHealth, UnitTypeLoader loader) {
		this.animContainer = loader;
		this.maxHealth = maxHealth;
	}

	public int getMaxHealth() {
		return maxHealth;
	}
	
}

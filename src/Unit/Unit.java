package Unit;

import Animation.Animatable;
import Animation.Animation;
import Bars.Bar;
import Bars.BarType;
import Main.Main;
import Player.Playable;
import Player.Player;
import Weapon.Weapon;
import Weapon.WeaponType;
import World.Direction;
import World.Location;
import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Units.Vector;

public class Unit extends Animatable {
	
	private int health;
	private double speed;
	private Direction facingDirection;
	private Bar healthBar;
	
	//Movement
	private Direction moveDirection = Direction.NONE;
	private int timeSinceMovementStart; 
	private Vector velocity;
	private Location movementStartLoc;
	private double timeToMoveOneLocation;
	private int unitId;
	
	private int maxMoveChangeCooldown = 10;
	private int moveChangeCooldown = maxMoveChangeCooldown;
	private int maxAttackCooldown = 50;
	private int attackCooldown;
	private Unit targetUnit;
	private int ticksSinceDetect;
	private int ticksUntilAttackAfterDetect = 30;
	private int minDistanceToTarget = 10;
	
	private Location location;
	private UnitType type;
	
	private int moveCooldown = 0;
	
	public Unit(UnitType type, Location location){
		this.location = location;
		this.type = type;
		this.health = type.getMaxHealth();
		this.speed = 2;
		this.facingDirection = Direction.SOUTH;
		this.velocity = new Vector();
		this.timeToMoveOneLocation = 1000 / speed;
		this.healthBar = new Bar(BarType.HealthBar, 0, type.getMaxHealth(), health);
		this.unitId = Main.getWorld().getFreeUnitId();
	}
	
	public void tick(int timeSinceLastUpdate){
		if(this instanceof Playable){
			Playable playable = (Playable)this;
			playable.checkActions();
			if(currentAnimation == null){
				currentAnimation = getCurrentIdleAnimationReversed();
			}
		} else {
			if(currentAnimation == null){
				currentAnimation = getCurrentIdleAnimation();
			}
			if(moveChangeCooldown > 0){
				moveChangeCooldown--;
			} else {
				Unit target = doWatch();
				Direction dir;
				if(target == null){
					dir = Direction.values()[(int)(Math.random() * Direction.values().length)];
				} else {
					if(getDistanceToUnit(target) <= minDistanceToTarget){
						dir = Direction.NONE;
						setFacingDirection(getUnitDirRelativeToThis(target));
					} else {
						dir = getUnitDirRelativeToThis(target);
						setFacingDirection(dir);
					}
				}
				velocity.set(dir.getXMod(), dir.getYMod());
				moveChangeCooldown = maxMoveChangeCooldown;
			}
			if(attackCooldown == 0){
				Unit detectedUnit = null;
				for(int i = 0; i < 5; i++){
					Unit unit = Main.getWorld().getUnitAt(new Location(location.getXAsInt() + facingDirection.getXMod() * i, location.getY() + facingDirection.getYMod() * i));
					if(unit != null && unit instanceof Playable){
						detectedUnit = unit;
						if(targetUnit != unit){
							targetUnit = unit;
						} else {
							if(ticksSinceDetect < ticksUntilAttackAfterDetect){
								ticksSinceDetect++;
							} else {
								attack();
								moveChangeCooldown = maxMoveChangeCooldown;
							}
						}
						break;
					}
				}
				if(detectedUnit == null){
					targetUnit = null;
					ticksSinceDetect = 0;
				}
			} else {
				attackCooldown -= 1;
			}
		}
		updateAnimation();
		updateLocation(timeSinceLastUpdate);
		healthBar.update();
	}

	public void render(Screen screen){
		if(currentAnimation != null){
			currentAnimation.render(screen, currentAnimState, location.toPixelPosX(), location.toPixelPosY());
			healthBar.renderBar(screen, location.toPixelPosX(), location.toPixelPosY());
		}
	}

	public void updateLocation(int timeSinceLastUpdate){
		Direction currentDir = Direction.toDirection(velocity.getX(), velocity.getY());
		if(moveCooldown > 0){
			moveCooldown--;
		}
		if(moveDirection == Direction.NONE){
			if(currentDir != Direction.NONE){
				if(facingDirection == currentDir){
					if(moveCooldown == 0){
						if(onMovementStart()){
							moveDirection = currentDir;
							movementStartLoc = new Location(location.getXAsInt(), location.getYAsInt());
						}
					}
				} else {
					facingDirection = currentDir;
					moveCooldown = 5;
				}
			}
		} else {
			timeSinceMovementStart += timeSinceLastUpdate;
			if(timeSinceMovementStart >= timeToMoveOneLocation){
				location.set(movementStartLoc.getXAsInt() + moveDirection.getXMod(), movementStartLoc.getYAsInt() + moveDirection.getYMod());
				timeSinceMovementStart = 0;
				moveDirection = Direction.NONE;
				movementStartLoc = location;
				onMovementFinish();
			} else {
				location.set(movementStartLoc.getXAsInt() + moveDirection.getXMod() * (1 / timeToMoveOneLocation * timeSinceMovementStart), movementStartLoc.getYAsInt() + moveDirection.getYMod() * ((1 / timeToMoveOneLocation) * timeSinceMovementStart));
			}
		}
	}
	
	public boolean onMovementStart(){
		if(Main.getWorld().getTerrainAt(location.getXAsInt() + facingDirection.getXMod(), location.getYAsInt() + facingDirection.getYMod()).getType().isSolid()){
			return false;
		}
		if(this instanceof Playable){
			currentAnimation = getCurrentWalkAnimationReversed();
		} else {
			currentAnimation = getCurrentWalkAnimation();
		}
		return true;
	}
	
	public void onMovementFinish(){
		
	}
	
	private Unit doWatch(){
		int xStart = location.getXAsInt();
		int yStart = location.getYAsInt();
		int xEnd = xStart + facingDirection.getXMod() * 3;
		int yEnd = yStart + facingDirection.getYMod() * 3;
		if(xEnd != xStart){
			yStart -= 3;
			yEnd += 3;
		} else {
			xStart -= 3;
			xEnd += 3;
		}
		if(xStart > xEnd){
			int x = xStart;
			xStart = xEnd;
			xEnd = x;
		}
		if(yStart > yEnd){
			int y = yStart;
			yStart = yEnd;
			yEnd = y;
		}
		for(int x = xStart; x <= xEnd; x++){
			for(int y = yStart; y <= yEnd; y++){
				Unit unit = Main.getWorld().getUnitAt(new Location(x, y));
				if(unit != null && unit instanceof Playable){
					return unit;
				}
			}
		}
		return null;
	}
	
	private Direction getUnitDirRelativeToThis(Unit unit){
		return Direction.toDirection(unit.getLocation().getX() - location.getX(), unit.getLocation().getY() - location.getY());
	}
	
	private double getDistanceToUnit(Unit unit){
		return (Math.abs(unit.getLocation().getX() - location.getX()) + Math.abs(unit.getLocation().getY() + location.getY())) / 2;
	}
	
	public void attack(){
		Weapon weapon = new Weapon(WeaponType.FIREBALL, new Location(location.getX() + 0.25, location.getY() + 0.25), new Vector((double)getFacingDirection().getXMod() / 7, (double)getFacingDirection().getYMod() / 7));
		weapon.setOwner(this);
		attackCooldown += maxAttackCooldown;
	}
	
	public void heal(int health){
		this.health += health;
		if(this.health > type.getMaxHealth()){
			health = type.getMaxHealth();
		}
		healthBar.setValue(health);
	}
	
	@Override
	protected void onAnimFinish(Animation animation) {
		if(animation != type.animContainer.idleDownAnimation || animation != type.animContainer.idleLeftAnimation || animation != type.animContainer.idleUpAnimation || animation != type.animContainer.idleRightAnimation){
			if(this instanceof Playable){
				currentAnimation = getCurrentIdleAnimationReversed();
			} else {
				currentAnimation = getCurrentIdleAnimation();
			}
		}
	}
	
	public void damage(int damage) {
		health -= damage;
		healthBar.setValue(health);
		if(health <= 0){
			if(this instanceof Playable){
				((Player)this).onDeath();
			} else {
				Main.getWorld().removeUnit(this);
			}
		}
	}
	
	//Originals
	public Animation getCurrentIdleAnimation(){
		if(facingDirection == Direction.NONE || facingDirection == Direction.SOUTH){
			return type.animContainer.idleDownAnimation;
		} else if(facingDirection == Direction.WEST){
			return type.animContainer.idleLeftAnimation;
		} else if(facingDirection == Direction.NORTH){
			return type.animContainer.idleUpAnimation;
		} else if(facingDirection == Direction.EAST){
			return type.animContainer.idleRightAnimation;
		}
		return null;
	}
	
	public Animation getCurrentWalkAnimation(){
		if(facingDirection == Direction.NONE || facingDirection == Direction.SOUTH){
			return type.animContainer.walkDownAnimation;
		} else if(facingDirection == Direction.WEST){
			return type.animContainer.walkLeftAnimation;
		} else if(facingDirection == Direction.NORTH){
			return type.animContainer.walkUpAnimation;
		} else if(facingDirection == Direction.EAST){
			return type.animContainer.walkRightAnimation;
		}
		return null;
	}
	
	//Reversed
	
	public Animation getCurrentIdleAnimationReversed(){
		if(facingDirection == Direction.NONE || facingDirection == Direction.SOUTH){
			return type.animContainer.idleUpAnimation;
		} else if(facingDirection == Direction.WEST){
			return type.animContainer.idleRightAnimation;
		} else if(facingDirection == Direction.NORTH){
			return type.animContainer.idleDownAnimation;
		} else if(facingDirection == Direction.EAST){
			return type.animContainer.idleLeftAnimation;
		}
		return null;
	}
	
	public Animation getCurrentWalkAnimationReversed(){
		if(facingDirection == Direction.NONE || facingDirection == Direction.SOUTH){
			return type.animContainer.walkUpAnimation;
		} else if(facingDirection == Direction.WEST){
			return type.animContainer.walkRightAnimation;
		} else if(facingDirection == Direction.NORTH){
			return type.animContainer.walkDownAnimation;
		} else if(facingDirection == Direction.EAST){
			return type.animContainer.walkLeftAnimation;
		}
		return null;
	}

	//Getters and setters
	public Location getLocation(){
		return location;
	}
	
	public UnitType getType(){
		return type;
	}
	
	public int getHealth(){
		return health;
	}
	
	public void setSpeed(double speed){
		this.speed = speed;
		this.timeToMoveOneLocation = 1000 / speed;
	}

	public Direction getFacingDirection(){
		return facingDirection;
	}
	
	public void setFacingDirection(Direction facingDirection){
		this.facingDirection = facingDirection;
	}

	public double getSpeed(){
		return speed;
	}
	
	public Vector getVelocity(){
		return velocity;
	}
	
	public int getUnitId(){
		return unitId;
	}
	
}

package Main;

import java.awt.Color;

import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Graphics.SpriteSheet;
import de.Major_Sauce.Graphics.Tile;

public class MainMenu {

	private Screen screen;
	private int[] pixels;
	private int[] originalPixels;
	private BackgroundTile[] bgTiles;
	private int xTiles;
	private int yTiles;

	private Tile title_0;
	private Tile title_1;
	private Tile title_2;
	private Tile author;
	
	private enum BackgroundTile {
		
		STONE_WALL(0), STONE_WALL_SIDES(1), STONE_WALL_TORCH(2), MOSSY_STONE_WALL(3), MOSSY_STONE_WALL_TORCH(4), STONE_WALL_BOTTOM(5), GROUND(10);
		
		private static SpriteSheet sheet;

		protected Tile texture;
		private BackgroundTile(int tileNumber){
			loadSheet();
			this.texture = getTerrainTexture(tileNumber);
		}
		
		private static void loadSheet(){
			if(sheet != null){
				return;
			}
			sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/terrain.png", SystemData.tileWidth, SystemData.tileHeight);
		}
		
		private Tile getTerrainTexture(int tileNum){
			return sheet.getTile(tileNum, 2);
		}
	}
	
	double multi = 0;
	double shaderIncrease;
	int ticksSinceMultiSet = 10;
	
	public MainMenu(Screen menuScreen){
		this.screen = menuScreen;
		this.pixels = screen.getPixels();
		this.xTiles = screen.getWidth() / SystemData.tileWidth;
		this.yTiles = screen.getHeight() / SystemData.tileHeight;
		this.bgTiles = new BackgroundTile[xTiles * yTiles];
		
		SpriteSheet titleSheet = Main.spriteSheetManager.loadSpriteSheet("/textures/title.png", 320, 32);
		title_0 = titleSheet.getTile(0);
		title_1 = titleSheet.getTile(1);
		title_2 = titleSheet.getTile(2);
		author = titleSheet.getTile(3);
		
		update();
	}
	
	public void update(){
		for(int x = 0; x < xTiles; x++){
			for(int y = 0; y < yTiles; y++){
				int random = (int)(Math.random() * 5);
				if(random == 0){
					bgTiles[x + y * xTiles] = BackgroundTile.GROUND;
				} else if(random == 1) {
					bgTiles[x + y * xTiles] = BackgroundTile.MOSSY_STONE_WALL;
				} else if(random == 2) {
					bgTiles[x + y * xTiles] = BackgroundTile.MOSSY_STONE_WALL_TORCH;
				} else if(random == 3) {
					bgTiles[x + y * xTiles] = BackgroundTile.STONE_WALL;
				} else if(random == 4) {
					bgTiles[x + y * xTiles] = BackgroundTile.STONE_WALL_TORCH;
				} else if(random == 5) {
					bgTiles[x + y * xTiles] = BackgroundTile.STONE_WALL_SIDES;
				}
			}
		}
		for(int x = 0; x < xTiles; x++){
			for(int y = 0; y < yTiles; y++){
				screen.renderTile(bgTiles[x + y * xTiles].texture, x * SystemData.tileWidth * 2, y * SystemData.tileHeight * 2);
			}
		}
		this.originalPixels = pixels.clone();
	}
	
	public void render(){
		Color yellow = Color.BLACK;
		int rYellow = yellow.getRed();
		int gYellow = yellow.getGreen();
		int bYellow = yellow.getBlue();
		multi += shaderIncrease;
		if(multi >= 2.5){
			shaderIncrease = -0.001;
		} else if(multi <= 0.01){
			shaderIncrease = 0.001;
		}
		int index = 0;
		for(int col : originalPixels){
			Color color = new Color(col);
			int r = (int) (color.getRed() * multi + rYellow * multi);
			int g = (int) (color.getGreen() + gYellow * multi);
			int b = (int) (color.getBlue() + bYellow * multi);
			if(r > 255){
				r = 255;
			}
			if(g > 255){
				g = 255;
			}
			if(b > 255){
				b = 255;
			}
			pixels[index] = new Color(r, g, b).getRGB();
			index++;
		}
		screen.renderTile(title_0, 100, 50);
		screen.renderTile(title_1, 100, 82);
		screen.renderTile(title_2, 100, 112);
		screen.renderTile(author, 250, 350);
	}
	
	public Screen getScreen(){
		return screen;
	}
	
}

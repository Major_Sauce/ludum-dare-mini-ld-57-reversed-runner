package Main;

import java.awt.event.KeyEvent;

import javax.swing.JFrame;

import Player.Player;
import Unit.UnitType;
import World.Location;
import World.World;
import de.Major_Sauce.Game.Game;
import de.Major_Sauce.Game.InputManager.Key;
import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Graphics.Tile;
import de.Major_Sauce.IndieEngine.IndieEngine;

public class Main extends Game {
	private static final long serialVersionUID = 1L;
	
	private MainMenu mainMenu;
	private WinScreen winScreen;
	private Screen screen;
	private Tile tv_frame;
	private Screen currentScreen;
	private static World world;
	private static Player player;
	private Timer timer;
	
	private Key play;
	private Key exit;
	
	@Override
	public void preInit() {
		//Debugging
		systemVars.debuggFrameLimit = false;
		systemVars.debuggUpdateLimit = true;
		systemVars.debuggMode = true;
		
		//Inits
		systemVars.name = "Reversed Fighter";
		systemVars.version = "0.0.1";
		systemVars.preferredFPS = 60;
		systemVars.preferredUPS = 60;
		systemVars.buffers = 2;
		systemVars.defaultCloseOperation = JFrame.EXIT_ON_CLOSE;
		systemVars.width = 832;
		systemVars.height = 468;
		systemVars.resizable = false;
		systemVars.undecorated = true;
		systemVars.showFps = false;
		systemVars.transparentColor = -65316;
	}

	@Override
	public void afterInit() {	
		tv_frame = spriteSheetManager.loadSpriteSheet("/textures/tv-frame.png", 832, 468).getTile(0);
		timer = new Timer();
		
		//Init keys
		play = inputManager.createKey(KeyEvent.VK_SPACE);
		exit = inputManager.createKey(KeyEvent.VK_ESCAPE);
		
		mainMenu = new MainMenu(screenManager.createScreen(systemVars.transparentColor, systemVars.width, systemVars.height));
		winScreen = new WinScreen(screenManager.createScreen(systemVars.transparentColor, systemVars.width, systemVars.height), timer);
		currentScreen = mainMenu.getScreen();
		screen = screenManager.createScreen(systemVars.transparentColor, systemVars.width, systemVars.height);
		world = new World(17, 17);
		player = new Player(this, UnitType.PLAYER, new Location(7, 7));
		world.addUnit(player);
		world.spawnEnemys(5);
	}

	@Override
	public void update(int pastSinceLastUpdate) {
		if(exit.isPressed()){
			System.exit(0);
		}
		if(play != null && play.isPressed()){
			currentScreen = screen;
			timer.start();
			play = null;
		}
		if(currentScreen == screen){
			screen.getOffset().set(player.getLocation().toPixelPosX() - systemVars.width / 2 + 32, player.getLocation().toPixelPosY() - systemVars.height / 2 + 32);
			world.update(pastSinceLastUpdate);
			timer.update(pastSinceLastUpdate);
		}
	}

	@Override
	public void render(int[] pixels) {
		if(currentScreen == screen){
			currentScreen.wipe();
			world.render(currentScreen);
			timer.render(currentScreen);
		} else if(currentScreen == mainMenu.getScreen()){
			mainMenu.render();
		} else if(currentScreen == winScreen.getScreen()){
			winScreen.render();
		}
		currentScreen.renderTile(tv_frame, currentScreen.getOffset().getXAsInt(), currentScreen.getOffset().getYAsInt());
		currentScreen.render(pixels);
	}

	//INIT
	public static void main(String[] args) {
		new Main();
	}
	
	public Main(){
		IndieEngine engine = new IndieEngine();
		engine.start(this);
	}
	
	public static World getWorld(){
		return world;
	}
	
	public static Player getPlayer(){
		return player;
	}
	
	public void won(){
		timer.stop();
		currentScreen = winScreen.getScreen();
	}
	
}

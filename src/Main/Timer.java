package Main;

import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Graphics.SpriteSheet;
import de.Major_Sauce.Graphics.Tile;

public class Timer {

	private boolean running = false;
	private int currentTimeMs;
	private int sec;
	private int min;
	
	private int xDraw = 250;
	private int yDraw = 50;
	
	private Tile int_0;
	private Tile int_1;
	private Tile int_2;
	private Tile int_3;
	private Tile int_4;
	private Tile int_5;
	private Tile int_6;
	private Tile int_7;
	private Tile int_8;
	private Tile int_9;
	
	public Timer(){
		SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/timerFonts.png", 32, 32);
		int_0 = sheet.getTile(0);
		int_1 = sheet.getTile(1);
		int_2 = sheet.getTile(2);
		int_3 = sheet.getTile(3);
		int_4 = sheet.getTile(4);
		int_5 = sheet.getTile(5);
		int_6 = sheet.getTile(6);
		int_7 = sheet.getTile(7);
		int_8 = sheet.getTile(8);
		int_9 = sheet.getTile(9);
	}
	
	public void start(){
		running = true;
	}
	
	public void stop(){
		running = false;
	}
	
	public void setXDraw(int xDraw){
		this.xDraw = xDraw;
	}
	
	public void setYDraw(int yDraw){
		this.yDraw = yDraw;
	}
	
	public void update(int timeSinceLastUpdate){
		if(!running){
			return;
		}
		currentTimeMs += timeSinceLastUpdate;
		if(currentTimeMs >= 1000){
			sec += 1;
			if(sec >= 60){
				min++;
				sec -= 60;
			}
			if(min >= 60){
				System.exit(0);
			}
			currentTimeMs -= 1000;
		}
	}
	
	public void render(Screen screen){
		int screenOffsetX = screen.getOffset().getXAsInt();
		int screenOffsetY = screen.getOffset().getYAsInt();		
		screen.renderTile(getNumTile((int)(min / 10)), screenOffsetX + xDraw + 160, yDraw + screenOffsetY);
		screen.renderTile(getNumTile((int)(min % 10)), screenOffsetX + xDraw + 192, yDraw + screenOffsetY);
		screen.renderTile(getNumTile((int)(sec / 10)), screenOffsetX + xDraw + 234, yDraw + screenOffsetY);
		screen.renderTile(getNumTile((int)(sec % 10)), screenOffsetX + xDraw + 266, yDraw + screenOffsetY);
	}
	
	private Tile getNumTile(int num){
		switch (num) {
		case 0:
			return int_0;
		case 1:
			return int_1;
		case 2:
			return int_2;
		case 3:
			return int_3;
		case 4:
			return int_4;
		case 5:
			return int_5;
		case 6:
			return int_6;
		case 7:
			return int_7;
		case 8:
			return int_8;
		case 9:
			return int_9;
		default:
			return null;
		}
	}
	
}

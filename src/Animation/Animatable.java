package Animation;

public abstract class Animatable {

	public int currentAnimState;
	public Animation currentAnimation;	
	private int ticksSinceLastUpdate;
	
	protected abstract void onAnimFinish(Animation animation);
	
	public void updateAnimation(){
		if(currentAnimation == null){
			System.out.println("Current animation == null");
			return;
		}
		if(ticksSinceLastUpdate < currentAnimation.getTicksPerUpdate()){
			ticksSinceLastUpdate++;
		} else {
			ticksSinceLastUpdate = 0;
			currentAnimState++;
			if(currentAnimState >= currentAnimation.getMaxIndex()){
				currentAnimState = 0;
				onAnimFinish(currentAnimation);
			}
		}
	}
	
}

package Animation;

public abstract class AnimationContainer {

	public AnimationContainer(){
		loadAnimations();
	}
	
	public abstract void loadAnimations();
	
}

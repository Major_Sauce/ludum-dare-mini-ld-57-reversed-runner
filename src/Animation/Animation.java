package Animation;

import java.util.ArrayList;

import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Graphics.Tile;

public abstract class Animation {

	private ArrayList<Tile> animTiles;
	private int ticksPerUpdate;
	
	public Animation(){
		animTiles = new ArrayList<Tile>();
		loadAnimation();
	}
	
	public void addTile(Tile tile){
		animTiles.add(tile);
	}
	
	public Tile getTile(int index){
		if(index < 0 || index >= animTiles.size()){
			return null;
		}
		return animTiles.get(index);
	}
	
	public abstract void loadAnimation();
	
	public void render(Screen screen, int tileIndex, int x, int y){
		if(tileIndex >= animTiles.size() || tileIndex < 0){
			return;
		}
		screen.renderTile(animTiles.get(tileIndex), x, y);
	}
	
	public void setTicksPerUpdate(int updatesPerSwitch){
		this.ticksPerUpdate = updatesPerSwitch;
	}
	
	public int getTicksPerUpdate(){
		return ticksPerUpdate;
	}
	
	public int getMaxIndex(){
		return animTiles.size();
	}
	
}

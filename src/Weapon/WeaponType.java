package Weapon;

public enum WeaponType {

	FIREBALL(10, 50, 5, new FireballTypeLoader());
	
	private int baseDamage;
	private int critChance;
	private int critMultiplier;
	public WeaponAnimationContainer animContainer;
	
	private WeaponType(int baseDamage, int critChance, int critMultiplier, WeaponTypeLoader loader){
		this.animContainer = loader;
		this.baseDamage = baseDamage;
		this.critChance = critChance;
		this.critMultiplier = critMultiplier;
	}
	
	public int getCritChance(){
		return critChance;
	}
	
	public int getBaseDamage(){
		return baseDamage;
	}
	
	public int getCritMultiplier(){
		return critMultiplier;
	}
	
}

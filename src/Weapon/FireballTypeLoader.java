package Weapon;

import de.Major_Sauce.Graphics.SpriteSheet;
import Animation.Animation;
import Main.Main;

public class FireballTypeLoader extends WeaponTypeLoader {

	@Override
	public void loadAnimations() {
		
		final SpriteSheet sheet = Main.spriteSheetManager.loadSpriteSheet("/textures/attacks.png", 32, 32);
		
		fireballAnimation = new Animation() {
			@Override
			public void loadAnimation() {
				setTicksPerUpdate(5);
				addTile(sheet.getTile(0));
				addTile(sheet.getTile(1));
				addTile(sheet.getTile(2));
				addTile(sheet.getTile(3));
				addTile(sheet.getTile(4));
				addTile(sheet.getTile(5));
				addTile(sheet.getTile(6));
			}
		};
		
	}

}

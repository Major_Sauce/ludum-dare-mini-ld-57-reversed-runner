package Weapon;

import Animation.Animation;
import Animation.AnimationContainer;

public abstract class WeaponAnimationContainer extends AnimationContainer {

	public Animation fireballAnimation;
	
	@Override
	public abstract void loadAnimations();

}

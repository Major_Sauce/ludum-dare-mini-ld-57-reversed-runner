package Weapon;

import de.Major_Sauce.Game.ScreenManager.Screen;
import de.Major_Sauce.Units.Vector;
import Animation.Animatable;
import Animation.Animation;
import Main.Main;
import Player.Playable;
import Unit.Unit;
import World.Direction;
import World.Location;

public class Weapon extends Animatable {

	private Location startLoc;
	private Location location;
	private Vector velocity;
	private WeaponType type;
	private int weaponId;
	private Unit owner;
	
	public Weapon(WeaponType type, Location loc, Vector velocity){
		this.type = type;
		this.startLoc = new Location(loc.getXAsInt(), loc.getYAsInt());
		this.location = loc;
		this.velocity = velocity;
		this.weaponId = Main.getWorld().getFreeWeaponId();
		Main.getWorld().addWeapon(this);
		currentAnimation = type.animContainer.fireballAnimation;
	}
	
	public void tick(int timeSinceLastUpdate){
		location.move(velocity);
		if(Main.getWorld().getTerrainAt(location.getXAsInt(), location.getYAsInt()).getType().isSolid()){
			velocity = new Vector();
		} else {
			Unit unit = Main.getWorld().getUnitAt(location);
			if(unit != null && unit != owner){
				velocity = new Vector();
				if(!(unit instanceof Playable)){
					unit.setFacingDirection(getDamageDir());	
				}
			}
		}
		updateAnimation();
	}
	
	public void render(Screen screen){
		if(currentAnimation != null){
			currentAnimation.render(screen, currentAnimState, location.toPixelPosX(), location.toPixelPosY());
		}
	}
	
	//Getters
	public WeaponType getType(){
		return type;
	}

	private Direction getDamageDir(){
		return Direction.toDirection(startLoc.getX() - location.getX(), startLoc.getY() - location.getY());
	}
	
	@Override
	protected void onAnimFinish(Animation animation) {
		Unit unit = Main.getWorld().getUnitAt(location);
		if(unit != null){
			int damage = type.getBaseDamage();
			if((int)(Math.random() * 100) <= type.getCritChance()){
				damage *= type.getCritMultiplier();
				System.out.println("Crit");
			}
			unit.damage(damage);
		}
		Main.getWorld().removeWeapon(this);	
	}
	
	public int getWeaponId(){
		return weaponId;
	}
	
	public void setOwner(Unit unit){
		this.owner = unit;
	}
	
}

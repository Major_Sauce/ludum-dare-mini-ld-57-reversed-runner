package Weapon;

public abstract class WeaponTypeLoader extends WeaponAnimationContainer {

	@Override
	public abstract void loadAnimations();
	
}

package Player;

public interface Playable {
	
	public void checkActions();
	public void onDeath();
	
}

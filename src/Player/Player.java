package Player;

import java.awt.event.KeyEvent;

import de.Major_Sauce.Game.InputManager.Key;
import de.Major_Sauce.Units.Vector;
import Main.Main;
import Unit.Unit;
import Unit.UnitType;
import Weapon.Weapon;
import Weapon.WeaponType;
import World.Location;

public class Player extends Unit implements Playable {
	
	private Key moveDown;
	private Key moveLeft;
	private Key moveUp;
	private Key moveRight;
	private Key action1;
	
	private Main main;
	private int action1Cooldown;
	
	public Player(Main main, UnitType type, Location location) {
		super(type, location);
		this.main = main;
		
		moveDown = Main.inputManager.createKey(KeyEvent.VK_S);
		moveUp = Main.inputManager.createKey(KeyEvent.VK_W);
		moveLeft = Main.inputManager.createKey(KeyEvent.VK_A);
		moveRight = Main.inputManager.createKey(KeyEvent.VK_D);
		
		action1 = Main.inputManager.createKey(KeyEvent.VK_SPACE);
		
		setSpeed(2);
		
		System.out.println("Loaded the player");
	}
	
	@Override
	public void checkActions(){
		int x = 0;
		int y = 0;
		if(action1Cooldown > 0){
			action1Cooldown--;
		}
		if(action1.isPressed()){
			if(action1Cooldown == 0){
				action1Cooldown = 50;
				Weapon weapon =new Weapon(WeaponType.FIREBALL, new Location(getLocation().getX() + 0.25, getLocation().getY() + 0.25), new Vector((double)getFacingDirection().getXMod() / 7 * -1, (double)getFacingDirection().getYMod() / 7 * -1));
				weapon.setOwner(this);
			}
		}
		if(moveDown.isPressed()){
			y++;
		}
		if(moveUp.isPressed()){
			y--;
		}
		if(moveLeft.isPressed()){
			x--;
		}
		if(moveRight.isPressed()){
			x++;
		}
		getVelocity().set(x, y);
		
		for(Unit unit : Main.getWorld().getUnits()){
			if(unit != null){
				if(!(unit instanceof Playable)){
					return;
				}
			}	
		}
		main.won();
		Main.getWorld().removeUnit(this);
	}

	@Override
	public void onDeath() {
		if(Math.random() >= 0.5){
			heal(UnitType.PLAYER.getMaxHealth());
			Main.getWorld().spawnEnemys(5);
		}
	}
	
}